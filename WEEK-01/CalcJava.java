//3 week1
import java.util.*;
public class CalcJava
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter first number : ");
        int num1 = input.nextInt();
        System.out.println("Enter second number : ");
        int num2 = input.nextInt();
        System.out.println("Enter character : ");
        char c = input.next().charAt(0);
        if(c=='a')
        {
            System.out.println(num1+num2);
        }
        else if(c=='s')
        {
            System.out.println(num1-num2);
        }
        else if(c=='m')
        {
            System.out.println(num1*num2);
        }
        else if(c=='d')
        {
            System.out.println(num1/num2);
        }
        else
        {
            System.out.println(num1%num2);
        }

    }
}
/*
observation
    Here charAt(0) method is used to take a character from user,
    where next() method is used to take string from the user*/