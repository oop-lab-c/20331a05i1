// 10 week3
#include<iostream>
using namespace std;
class box
{
    private:
    int x;
    int y;
    int z;
    public:
    inline void welcome()
    {
        cout<<"Hai,this is inline function "<<endl;
    }
    void area(int a , int b)
    {
        x=a;
        y=b;
        cout<<"Area is "<<x*y<<endl;
    }
    friend void dis(box obj);
    void vo(int,int,int);
};
void box::vo(int a,int b,int c)
{
    x=a;
    y=b;
    z=c;
    cout<<"Volume is "<<x*y*z<<endl;
}
void dis(box obj)
{
    cout<<"The Dimensions are "<<endl;
    cout<<obj.x<<endl;
    cout<<obj.y<<endl;
    cout<<obj.z<<endl;
}
int main()
{
    int a= 5;
    int b= 10;
    int c= 20;
    box obj;
    obj.welcome();
    obj.area(a,b);
    obj.vo(a,b,c);
    dis(obj);
    return 0;

}
//observation
//here friend function is used to access the private variables of box