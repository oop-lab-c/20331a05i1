  //6 week2
#include<iostream>
using namespace std;
int main()
{
    int a,b;
    char op;
    cout<<"Enter the values of a and b"<<endl;
    cin>>a>>b;
    cout<<"Enter the operation of op"<<endl;
    cin>>op;
    switch(op)
    {
        case '+' : cout<<"addition is "<<a+b<<endl;
                   break;
        case '-' : cout<<"subtraction is "<<a-b<<endl;
                   break;
        case '*' : cout<<"multiplication is "<<a*b<<endl;
                   break;
        case '/' : cout<<"division is "<<a/b<<endl;
                   break;
        case '%' : cout<<"remainder is "<<a%b<<endl;
                   break;
        default  :
                   break;
    }
    return 0;

}
/* observation
    if we give float
    error: invalid operands of types 'float' and 'float' to binary 'operator%'
   21 |         case '%' : cout<<"remainder is "<<a%b<<endl;
*/
