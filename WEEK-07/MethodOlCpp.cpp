//26 week7
#include<iostream>
using namespace std;
class a
{
    public:
    void fun(int a,int b)
    {
        cout<<"ans1 is "<<a+b<<endl;
    }
    void fun(double a,double b)
    {
        cout<<"ans2 is "<<a+b<<endl;
    }
};
int main()
{
    a obj;
    obj.fun(2,3);
    obj.fun(2.22,3.33);
}
/*
observation:
here to achieve function overloading we have to change our datatypes of parameters list
 or change our no.of parameters  
*/
