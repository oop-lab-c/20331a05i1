//34 week 7
abstract class Impureabstraction{
    abstract void display();
}
class ParAbsJava extends Impureabstraction
{
    void display()
    {
        System.out.println("impure abstarction is achieved by abstract classes.");
    }
    public static void main(String[] args)
    {
        ParAbsJava obj = new ParAbsJava();
        obj.display();
    }
}
/*
observation
if we dont use abstract keyword in class,error occurs
java:1: error: Impureabstraction is not abstract and does not override abstract method display() in Impureabstraction
*/
