//37 week 9
public class ThreadClassJava extends Thread
{
    public void run()
    {
        for(int i=0;i<5;i++)
        {
            int a = 1;
            int b = 2;
            System.out.println(a+b);
        }
    }
    public static void main(String[] args)
    {
        ThreadClassJava obj = new ThreadClassJava();
        obj.start();
    }
}
/*
observation
if we cannot use public in run() method then error rises:
thready.java:3: error: run() in thready cannot implement run() in Runnable
    void run()
         ^
  attempting to assign weaker access privileges; was public
1 error
*/