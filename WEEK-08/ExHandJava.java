//35 week 8
class ExHandJava
{
        public static void main(String[] args)
    {
        int a = 35;
        int b = 0;
        int c ;
        try
        {
            c=a/b;
        }
        catch(ArithmeticException obj)
        {
            System.out.println(obj);
        }
    }
}
/*
observation
we can have chance to print the exception by printing the object present in try block
output
Builtinexception
java -cp /tmp/TIFWuVsKaCjava.lang.ArithmeticException: / by zero

*/