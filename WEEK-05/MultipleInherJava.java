//22 week5
interface a
{
    void age();
}
interface b
{
    void name();
}
class  c implements a,b 
{
    public void age()
    {
        System.out.println("My age is 20");
    }
    public void name()
    {
        System.out.println("My name is ramu");
    }
}
class MultipleInherJava
{
    public static void main(String[] args)
    {
        c obj = new c();
        obj.name();
        obj.age();
    }
}
