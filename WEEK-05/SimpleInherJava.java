//19 week5
class Parent
{
    void fun()
    {
        System.out.println("Hai , i am parent class");
    }
}
class SimpleInherJava extends Parent
{
    void fun1()
    {
        System.out.println("Hai , i am child class");
    }
    public static void main(String args[])
    {
        SimpleInherJava obj = new SimpleInherJava();
        obj.fun();
        obj.fun1();
    }
}
/*
observation
   This is simple inheritance because in this only one super class , one base classw was present.*/
