//14 week 4
class ParamConstJava
{
    String fullName;
    double semPercentage;
    String collegeName;
    int collegeCode;
    public ParamConstJava()
    {
         collegeName = "MVGR";
         collegeCode = 33;
         System.out.println("College name is "+collegeName);
         System.out.println("college code is "+collegeCode);
    }
    public ParamConstJava(String x, double y)
    {
        fullName=x;
        semPercentage=y;
    }
    void display()
    {
        System.out.println("name is "+fullName);
        System.out.println("semister percentage is "+semPercentage);
    }
    public static void main(String[] args)  
    {
        ParamConstJava obj = new ParamConstJava();
        ParamConstJava obj1 = new ParamConstJava("john",67.889);
        obj1.display();
    }
}
/*
observation
    Here there is no need to use a destructor to destroy a object because java has automatic destructor.
    if we want to pass the values to constructor , we used parametric constructor.
    if we dont assign the values to default constructor then it print default values null, 0.
    we created 2 objects for 2 constructors.*/
